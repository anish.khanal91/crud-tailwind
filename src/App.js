import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { AddUser } from "./pages/AddUser";
import { EditUser } from "./pages/EditUser";
import { Heading } from "./components/Heading";
import { Home } from "./components/Home";
import { UserList } from "./pages/UserList";

function App() {
  return (
    <div>
      <Heading/>
      <Router>
        <Routes>
        <Route exact path="/" element={<Home/>}/>
        <Route exact path="/user/:id" element={<UserList/>}/>
        <Route exact path="/add" element={<AddUser/>}/>
        <Route exact path="/edit/:id" element={<EditUser/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
